require('./bootstrap');
import 'bootstrap/dist/css/bootstrap.css'

window.Vue = require('vue').default;

import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';

Vue.use(VueMaterial);

import VueDirectiveMask from 'vue-directive-mask';
Vue.use(VueDirectiveMask);

import App from './App.vue'

const app = new Vue({
      render: h => h(App),
}).$mount('#app')
