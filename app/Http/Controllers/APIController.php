<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use Carbon\Carbon;

class APIController extends Controller
{
    public function getClients() {
        $clients = Client::get();
        return response()->json($clients);
    }

    public function saveClient(Request $request) {
        $result = Client::updateOrCreate(
            ['phone'=>$request->phone],
            ['name' => $request->name, 'birthdate'=>Carbon::parse($request->birthdate)->format("Y-m-d")]
        );
        return response()->json($result);
    }
}
